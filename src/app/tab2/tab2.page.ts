import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Settings } from '../private/settings';
///Servicios
import { PublishService } from '../services/publish/publish.service';
import { StorageService } from '../services/storage/storage.service';
import { FireService } from '../services/firebase/firebase.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  publications:any;
  uid:any;

  constructor(
    private publication: PublishService,
    private storage: StorageService,
    private userService:FireService,
    private router:Router
  ) { }

  ngOnInit() {

    this.uid = this.userService.getUid();
    this.publication.getPublications()
      .subscribe((response) => {
        this.publications = response;
        this.setContent();
      });

  }

  getImage(path: string) {
    return this.storage.getFile(path)
    .then((url) => {return url});
  }

  setContent() {
    for (let index = 0; index < this.publications.length; ++index) {
      this.getImage(this.publications[index].ref)
      .then((image) => {
        this.publications[index]["image"] = image;
      });
    }
  }

  sendInfo(title:string,body:string,image:string){
    Settings.setting = {title:title,body:body,image:image};
    this.router.navigateByUrl("/child-tab2");

  }


}







