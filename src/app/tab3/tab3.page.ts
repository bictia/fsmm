import { Component , OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Settings } from '../private/settings';

//Servicios
import { FireService } from '../services/firebase/firebase.service';
import { UserService } from '../services/backend/backend.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  //Atributos
  userElements: Array<UserElement>;
  userAuth: any;
  userDatabase: any;

  //Metodos
  constructor(
    private router:Router,
    private firebase: FireService,
    private backend: UserService
  ) {}

  ngOnInit() {
    this.userAuth = this.firebase.getUser();
    this.backend.getData(this.userAuth.uid)
    .subscribe(response => {
      this.userDatabase = response;
      this.userElements = [
        new UserElement("name", "Nombre", "auth", "person-circle", this.userAuth.displayName),
        new UserElement("id", "Cedula", "backend", "journal", this.userDatabase.id),
        new UserElement("email", "Correo Electronico", "auth", "mail", this.userAuth.email),
        new UserElement("password", "Contraseña", "auth", "shield", "Modifica tu contraseña"),
        new UserElement("phone", "Celular", "auth", "call", this.userAuth.phoneNumber),
        new UserElement("age", "Fecha de Nacimiento", "backend", "man", this.userDatabase.age),
        new UserElement("childsNumber", "Numero de hijos", "backend", "people", this.userDatabase.childsNumber),
        new UserElement("civilState", "Estado civil", "backend", "heart-half", this.userDatabase.civilState),
        new UserElement("info", "Descripción", "backend", "school", "Hablanos más sobre ti")
      ];
    });
  }

  editInfo(type: string, name: string, content: string) {
    Settings.setting = {name: name, type: type, content: content};
    this.router.navigateByUrl('/user-settings');
  }

  logout(): void {
    this.firebase.logout();
  }

}

class UserElement {

  //Atributos
  name: string;
  text: string;
  type: string;
  icon: string;
  value: any;

  //Constructor
  constructor(name: string, text: any, type: string, icon: string, value: any) {
    this.name = name;
    this.text = text;
    this.type = type;
    this.icon = icon;
    this.value = value;
  }
}