import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

//Servicios
import { FireService } from '../services/firebase/firebase.service';
import { UserService } from '../services/backend/backend.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  //Atributos
  isloginView: boolean;

  userInfo: UserInfo;

  pageInfo: string;

  //Metodos
  constructor(
    private firebase: FireService,
    private backend: UserService,
    private alertController: AlertController
    ) {  }

  async presentAlert(title: string, subtitle: string, message: string) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: message,
      buttons: ['Vale']
    });
  
    await alert.present();
  }

  ngOnInit() {
    this.userInfo = new UserInfo();
    this.loginView();
  }

  loginView(): void {
    this.pageInfo = "Iniciar Sesion"
    this.isloginView = true;
  }

  registerView(): void {
    this.pageInfo = "Registrase"
    this.isloginView = false;
  }

  login(): void {
    let iscomplete: boolean = !this.userInfo.email && !this.userInfo.password;
    if(!iscomplete) {
      this.firebase.login(this.userInfo.email, this.userInfo.password)
      .catch((error) => {
        switch (error.code) {
  
          case "auth/wrong-password":
            this.presentAlert("No Pudiste Iniciar Sesion", "Contraseña Incorrecta", "Prueba nuevamente");
            break;
        
          case "auth/user-not-found":
            this.registerView();
            break;
  
          default:
            this.presentAlert("Kernel Panic", "Error del Sistema", "El mundo se va a acabar, ¡CORRE!");
            break;
        }
      });
    } else {
      this.noInfoAlert();
    }
  }

  noInfoAlert(): void {
    this.presentAlert("Falta de Informacion", "No has suministrado toda la informacion requerida", "Completa todos los campos");
  }

  register(): void {
    let iscomplete: boolean = !this.userInfo.email && !this.userInfo.password && !this.userInfo.displayName && !this.userInfo.confirmPassword;
    if(!iscomplete) {
      if(this.userInfo.confirmPassword === this.userInfo.password) {
        this.backend.createUser({
          displayName: this.userInfo.displayName,
          email: this.userInfo.email,
          password: this.userInfo.password
        })
        .then(() => {
          this.login();
        })
        .catch((error) => {
          this.presentAlert("Error al Crear la Cuenta", error.code, error.message);
        });
      }
    } else {
      this.noInfoAlert();
    }
  }

}

class UserInfo {
  //Atributos
  public displayName: string;
  public email: string;
  public password: string;
  public confirmPassword: string;

  //Constructor
  constructor() {}
}