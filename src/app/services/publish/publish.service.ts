import { Injectable } from '@angular/core';
import { proxyConfig } from '../../private/proxy.config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PublishService {

  constructor(private http: HttpClient) { }

  public getPublications(){
    return this.http.get(proxyConfig.target + 'publications');
  }

  public createPublication(publication: any): any {
     return this.http.post(proxyConfig.target + 'publications', publication);
  }

  public getPublicationsByUser(uid: string): any {
    return this.http.get(proxyConfig.target + 'publications/' + uid);
  }

}

