import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { proxyConfig } from '../../private/proxy.config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) { }

  public getData(uid: string): any {
    return this.http.get(proxyConfig.target + 'user/firestore/' + uid);
  }

  public updateData(uid: string, data:any): any {
    return new Promise((resolve, rejected) => {
      this.http.put(proxyConfig.target + 'user/firestore/' + uid, data).toPromise()
      .then((response) => {resolve(response)})
      .catch((error) => {rejected(error)});
    });
  }

  public getUser(uid: string): any {
    return new Promise((resolve, rejected) => {
      this.http.get(proxyConfig.target + 'user/auth/' + uid).toPromise()
      .then((response) => {resolve(response)})
      .catch((error) => {rejected(error)});
    });
  }

  public createUser(user: any): any {
    return new Promise((resolve, rejected) => {
      this.http.post(proxyConfig.target + 'user/auth/', user).toPromise()
      .then((response) => {resolve(response)})
      .catch((error) => {rejected(error)});
    });
  }

  public updateUser(uid: string, user: any): any {
    return new Promise((resolve, rejected) => {
      this.http.put(proxyConfig.target + 'user/auth/' + uid, user).toPromise()
      .then((response) => {resolve(response)})
      .catch((error) => {rejected(error)});
    });
  }

  public deleteUser(uid: string): any {
    return new Promise((resolve, rejected) => {
      this.http.delete(proxyConfig.target + 'user/auth/' + uid).toPromise()
      .then((response) => {resolve(response)})
      .catch((error) => {rejected(error)});
    });
  }

}