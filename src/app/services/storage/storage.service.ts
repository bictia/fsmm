import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: AngularFireStorage) { }

  public setFile(path: string, file: any): any {
    return new Promise((resolve, rejected) => {
      this.storage.upload(path, file)
      .then((response) => resolve(response))
      .catch((error) => rejected(error));
    });
  }

  public getFile(path: string): any {
    
    return new Promise( (resolve,rejected) => {
      this.storage.storage.ref(path).getDownloadURL()
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        rejected(error);
      });
    });
      
    
  }

  public deleteFile(path: string): any {
    return new Promise((resolve, rejected) => {
      this.storage.storage.ref(path).delete()
      .then((response) => resolve(response))
      .catch((error) => rejected(error));
    });
  }

}
