import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FireService {

  constructor(
    private auth: AngularFireAuth,
    private storage: Storage,
    private router: Router
  ) {}

  //Metodos getter y setter

  public getName() {
    return this.auth.auth.currentUser.displayName;
  }

  public getUid(){
    return this.auth.auth.currentUser.uid;
  }

  //Metodos

  public login(email: string, password: string): any {
    return new Promise((resolve, rejected) => {
      this.auth.auth.signInWithEmailAndPassword(email, password)
      .then((response) => {
        this.router.navigateByUrl('/user');
        resolve(response);
      })
      .catch((error) => {
        rejected(error);
      });
    });
  }

  public logout(): any {
    return new Promise((rejected, resolve) => {
      this.auth.auth.signOut()
      .then((response) => {
        this.storage.clear();
        this.router.navigateByUrl('/');
        resolve(response);
      })
      .catch((error) => {
        rejected(error);
      });
    });
  }

  public getUser(): any {
    this.auth.auth.currentUser.reload();
    return this.auth.auth.currentUser;
  }

}
