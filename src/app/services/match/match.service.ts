import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { proxyConfig } from '../../private/proxy.config';

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor(private http: HttpClient) { }

  public getMatch(publicationId: string) {
    return this.http.get(proxyConfig.target + 'match/' + publicationId);
  }

  public updateMatch(publicationId: string, data: any) {
    return this.http.put(proxyConfig.target + 'match/' + publicationId, data).
      subscribe( (response) => {
        console.log(response);
      });
  }

}
