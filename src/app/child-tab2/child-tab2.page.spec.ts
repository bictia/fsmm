import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChildTab2Page } from './child-tab2.page';

describe('ChildTab2Page', () => {
  let component: ChildTab2Page;
  let fixture: ComponentFixture<ChildTab2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildTab2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChildTab2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
