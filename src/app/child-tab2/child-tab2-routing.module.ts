import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChildTab2Page } from './child-tab2.page';

const routes: Routes = [
  {
    path: '',
    component: ChildTab2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChildTab2PageRoutingModule {}
