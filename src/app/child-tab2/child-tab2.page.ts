import { Component, OnInit } from '@angular/core';
import { Settings } from '../private/settings';
import { AngularFirestore } from '@angular/fire/firestore';
//Services
import { MatchService } from '../services/match/match.service';
import { FireService } from '../services/firebase/firebase.service';



@Component({
  selector: 'app-child-tab2',
  templateUrl: './child-tab2.page.html',
  styleUrls: ['./child-tab2.page.scss'],
})

export class ChildTab2Page implements OnInit {

   title:string;
   body:string;
   image:string;
   idPublication:any;
   send:any = {
     uid:""
   };

  constructor(private match:MatchService,private user:FireService,private database:AngularFirestore) { }

  ngOnInit() {
    this.title = Settings.setting.title;
    this.body = Settings.setting.body;
    this.image = Settings.setting.image;
    this.getIdPublication();
  }

  getIdPublication(){

    this.database.collection('publications').snapshotChanges()
      .forEach( (array) => {
        array.map( (data) => {
          if(data.payload.doc.data()["body"] === this.body && data.payload.doc.data()["title"] === this.title)
          {
            this.idPublication =  data.payload.doc.id;
          }
          
        });
      });

    
  }

  addParticipants(){
    this.send.uid = this.user.getUid();
    this.match.updateMatch(this.idPublication,this.send);
  }

  

 

}
