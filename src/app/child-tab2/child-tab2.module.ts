import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChildTab2PageRoutingModule } from './child-tab2-routing.module';

import { ChildTab2Page } from './child-tab2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChildTab2PageRoutingModule
  ],
  declarations: [ChildTab2Page]
})
export class ChildTab2PageModule {}
