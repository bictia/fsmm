import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//Servicios
import { StorageService } from '../services/storage/storage.service';
import { PublishService } from '../services/publish/publish.service';
import { FireService } from '../services/firebase/firebase.service';
import { Settings } from '../private/settings';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  publications = [];

  constructor(
    private publication: PublishService,
    private storage: StorageService,
    private firebase: FireService,
    private router: Router
  ) { }

  ngOnInit() {

    this.publication.getPublicationsByUser(this.firebase.getUid())
      .subscribe((response) => {
        this.publications = response;
        this.setContent();
      });

  }

  getImage(path: string) {
    return this.storage.getFile(path)
    .then((url) => {return url});
  }

  setContent() {
    for (let index = 0; index < this.publications.length; ++index) {
      this.getImage(this.publications[index].ref)
      .then((image) => {
        this.publications[index]["image"] = image;
      });
    }
  }

  getPublication(publicationId: string) {
    Settings.setting = publicationId;
    this.router.navigateByUrl('/list');
  }

}
