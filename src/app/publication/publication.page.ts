import { Component, OnInit } from '@angular/core';
import { FireService } from '../services/firebase/firebase.service';
import { PublishService } from '../services/publish/publish.service';
import { StorageService } from '../services/storage/storage.service';


@Component({
  selector: 'app-publication',
  templateUrl: './publication.page.html',
  styleUrls: ['./publication.page.scss'],
})
export class PublicationPage implements OnInit {

  file: any;

  post: any = {
    uid: "",
    title: "",
    body: ""
  }

  globalId:any;

  constructor(
    private service: FireService,
    private storageService:StorageService,
    private publishService:PublishService
  ) { }

  
  ngOnInit() {
  }

  uploadFile(event) {
    this.file = event.target.files[0];
  }

  upload() {
    this.post.uid = this.service.getUid();
    this.publishService.createPublication(this.post)
    .subscribe(response => {
      this.storageService.setFile(this.post.uid + '/' + response, this.file);
    });
  }

}
