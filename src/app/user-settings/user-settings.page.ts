import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController } from '@ionic/angular';
import * as firebaseSettings from 'firebase';
//Services
import { UserService } from '../services/backend/backend.service';
import { FireService } from '../services/firebase/firebase.service';
import { Settings } from '../private/settings';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.page.html',
  styleUrls: ['./user-settings.page.scss'],
})
export class UserSettingsPage implements OnInit {

  settings = Settings.setting;
  userAuth: any;
  userDatabse = {};
  userNewValue: any;
  userNewValueConfirm: any;

  constructor(
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    private backend:UserService,
    private firebase: FireService,
    private router:Router,
  ) {}

  async presentAlert(title: string, subtitle: string, message: string) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  ngOnInit() {
    this.userAuth = this.firebase.getUser();
    this.backend.getData(this.userAuth.uid)
    .subscribe((response) => {
      this.userDatabse = response;
    });
  }

  redirect():void {
    this.router.navigateByUrl("/user/account");
  }

  updateFirebase(): void {
    let inside: boolean = true;
    if(this.settings.name == "name") {
      this.backend.updateUser(this.userAuth.uid, {"displayName": this.userNewValue});
    } else if(this.settings.name == "email") {
      this.backend.updateUser(this.userAuth.uid, {"email": this.userNewValue});
    } else if(this.settings.name == "password") {
      if(this.userNewValue == this.userNewValueConfirm) {
        this.backend.updateUser(this.userAuth.uid, {"password": this.userNewValue});
      } else {
        inside = false;
        this.presentAlert("Contraseña", "Las contraseñas no coinciden", "Pusiste mal alguna contraseña, prueba nuevamente");
      }
    } else {
      this.backend.updateUser(this.userAuth.uid, {"phoneNumber": '+57' + this.userNewValue});
    }
    if(inside) {
      Settings.setting = null;
      this.redirect();
    }
  }

  updateBackend(): void {
    if(this.settings.name == "age") {
      this.backend.updateData(this.userAuth.uid, {"age": this.userNewValue});
    } else if(this.settings.name == "childsNumber") {
      this.backend.updateData(this.userAuth.uid, {"childsNumber": this.userNewValue});
    } else if(this.settings.name == "civilState") {
      this.backend.updateData(this.userAuth.uid, {"civilState": this.userNewValue});
    } else if(this.settings.name == "id") {
      this.backend.updateData(this.userAuth.uid, {"id": this.userNewValue});
    } else {
      this.backend.updateData(this.userAuth.uid, {"info": this.userNewValue});
    }
    Settings.setting = null;
    this.redirect();
  }

}
