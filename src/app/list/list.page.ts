import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { proxyConfig } from '../private/proxy.config';
import { AlertController } from '@ionic/angular';
//Servicios
import { MatchService } from '../services/match/match.service';
import { UserService } from '../services/backend/backend.service';
import { FireService } from '../services/firebase/firebase.service';
import { Settings } from '../private/settings';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  //Atributos
  userList = [];
  data:any = {
    from: 'managersmartmoney@gmail.com',
    to: '',
    subject: 'Solicitud de colaboracion',
    text: `${this.fireService.getName()} Acepto tu solicitud de trabajo.\nContactalo por medio de ${this.fireService.getUser().email} o por ${this.fireService.getUser().phoneNumber}.\nEl equipo de Smart Money Manager.`
  };

  constructor(
    public alertController: AlertController,
    private match: MatchService,
    private user: UserService,
    private http: HttpClient,
    private fireService:FireService
  ) { }

  async presentAlertConfirm(name: string, email: string) {
    const alert = await this.alertController.create({
      header: 'Contratar',
      message: 'Aceptas trabajar con ' + name,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Aceptar',
          handler: () => {
            this.sendEmail(email);
          }
        }
      ]
    });

    await alert.present();
  }


  ngOnInit() {
    this.match.getMatch(Settings.setting)
      .subscribe((response) => {
        if (response) {
          response["uid"].forEach(uid => {
            this.user.getUser(uid)
              .then((usuario) => {
                this.user.getData(uid)
                .subscribe((info) => {
                  usuario["firestore"] = info;
                  this.userList.push(usuario);
                });
              });
          });
        }
      });
  }

  sendEmail(to) {
    this.data.to = to;
    this.http.post(proxyConfig.target+'email/send', this.data)
      .subscribe( (response) => {
        console.log(response);
      });
  }

}
